
var r2bconfig = {
	url: {
		roominfo: "https://event.kif.rocks/custom-cgi/get-room-info",
		reactiondata: "https://event.kif.rocks/custom-cgi/get-reactionbar-data",
	},
	refreshtime: 2500,
	yaxessuggestedmax: 5,
	chartfontsize: 80,
};

var roomdata = null;
var reactiondata = [
	{ x: "🦙", emoj: "llama", y: 0, },
	{ x: "👏", emoj: "clap", y: 0, },
	{ x: "👍", emoj: "+1", y: 0 },
	{ x: "👎", emoj: "-1", y: 0 },
	{ x: "❤️", emoj: "heart", y: 0 },
	{ x: "❌", emoj: "x", y: 0 },
	{ x: "📢", emoj: "loudspeaker", y: 0 }
];
var reactiondataraw = null;
var chrt = null;

function addData(chart, label, data) {
    //chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    //chart.update();
}

function removeData(chart) {
    //chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    //chart.update();
}

function setAllReactionValues(val) {
	for (let i = 0; i < reactiondata.length; i++) {
		reactiondata[i].y = val;
	}
}

function addReactionValue(emoj,amount) {
	for (let i = 0; i < reactiondata.length; i++) {
		if (reactiondata[i].emoj === emoj) {
			reactiondata[i].y += amount;
			return;
		}
	}
}
function setReactionValue(emoj,val) {
	for (let i = 0; i < reactiondata.length; i++) {
		if (reactiondata[i].x === emoj) {
			reactiondata[i].y = val;
			return;
		}
	}
}
function getReactionValue(emoj) {
	for (let i = 0; i < reactiondata.length; i++) {
		if (reactiondata[i].x === emoj) {
			return reactiondata[i].y;
		}
	}
}

function updateReactionData(stat, resp) {
	reactiondataraw = resp.reactions;
	console.log(reactiondataraw);

	setAllReactionValues(0);
	reactiondataraw.forEach((react) => {
		addReactionValue(react.reaction, react.amount);
	});

	//removeData(chrt);
	//addData(chrt, r2bconfig.labels, reactiondata);

	//setReactionValue("heart", 20);

	chrt.update();
	console.log(chrt.data);
}

function updateRoomData(stat, resp) {
	roomdata = resp.rooms;
	console.log(stat);
	console.log(resp);
	console.log(roomdata);

}

function getJSON(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.responseType = 'json';
	xhr.onload = function() {
		var status = xhr.status;
		if (status === 200) {
			callback(null, xhr.response);
		} else {
			callback(status, xhr.response);
		}
	};
	xhr.send();
};


function createChart() {
	var ctx = document.getElementById('myChart').getContext('2d');
	return new Chart(ctx, {
		type: 'bar',
		data: {
			//labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
			datasets: [{
				label: '# of Reactions',
				//data: [12, 19, 3, 5, 2, 3],
				data: reactiondata,
				backgroundColor: [
					'rgba(255, 99, 132, 0.9)',
					'rgba(54, 162, 235, 0.9)',
					'rgba(255, 206, 86, 0.9)',
					'rgba(75, 192, 192, 0.9)',
					'rgba(153, 102, 255, 0.9)',
					'rgba(255, 159, 64, 0.9)'
				],
				borderColor: [
					'rgba(255, 99, 132, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				y: {
					beginAtZero: true,
					suggestedMax: r2bconfig.yaxessuggestedmax,
					ticks: {
						font: {
							size: r2bconfig.chartfontsize,
						}
					},
				},
				x: {
					ticks: {
						font: {
							size: r2bconfig.chartfontsize,
						}
					},
				},
			},
			// Boolean - whether or not the chart should be responsive and resize when the browser does.
			responsive: true,
			// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			maintainAspectRatio: true,

			plugins:{
				legend:{
					labels:{
						//Thismorespecificfontpropertyoverridestheglobalproperty
						font:{
							size: r2bconfig.chartfontsize,
						}
					}
				}
			}
		}
	});

}

function loop() {
	if (roomdata === null) {
		console.log("Waiting for roomdata...");
	} else {
		getJSON(r2bconfig.url.reactiondata, updateReactionData);
	}
}

window.onload = function(e){ 
	console.log("Using Config:");
	console.log(r2bconfig);
	chrt = createChart();
	getJSON(r2bconfig.url.roominfo, updateRoomData);
	loop();
	setInterval(loop, r2bconfig.refreshtime);
}

