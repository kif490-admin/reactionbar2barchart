
# reactionbar2barchart

Erstellt einen Barchart aus den Reactions in Venueless.

Dieses Programm muss auf dem Server auf dem auch Venueless läuft installiert werden. Zumindest die CGI Skripte müssen das.


Dieses Programm wurde explizit für die KIF 49,0 entwickelt und muss für eigenen Anwendungszwecke verändert werden.

## ACHTUNG

DIE CGI SKRIPTE IN [custom-cgi/](custom-cgi/) SIND NICHT SICHER!

1. Die CGI Skripte sind in Bash geschrieben. Es ist nie eine gute Idee für CGI Skripte Bash zu verwenden!
1. Die CGI Skripte können für einen [DOS-Angriff](https://de.wikipedia.org/wiki/Denial_of_Service) auf deinen Server missbraucht werden! Das häufige aufrufen der Skripte kann die Postgres Datenbank überlasten.

Des Weiteren beinhaltet dieses Programm im Allgemeinen viele Hacks welche überarbeitete werden müssen.

Die Veröffentlichung dieses Programms dient der Dokumentation. Gerne darfst du dieses Programm forken, anpassen & neu veröffentlichen.

**Ohne Änderungen, welche es sicherer machen, sollte es aber nie auf irgendwelchen Servern betrieben werden!**

## How-To-Install

1.:

```
sudo apt install npm libcurl4-openssl-dev fcgiwrap
```

2.:

```
git clone git@gitlab.gwdg.de:kif490-admin/reactionbar2barchart.git
cd reactionbar2barchart
npm install
```

3.:

Add the following to your nginx config:
```
location /reactionbar2barchart {
	# Path needs to be the path to the parent directory of the reactionbar2barchart directory
	# So if your reactionbar2barchart directory is /home/user/reactionbar2barchart/
	# then this line should look like this:
	# root /home/user;
	root   /path/to/the/parent/directoy;
	index  index.html;
}
location /custom-cgi/ {
	# Disable gzip (it makes scripts feel slower since they have to complete
	# before getting gzipped)
	gzip off;

	# Path needs to be pointing to the reactionbar2barchart directory.
	root   /path/to/reactionbar2barchart;

	fastcgi_pass  unix:/var/run/fcgiwrap.socket;
	include /etc/nginx/fastcgi_params;
	fastcgi_param SCRIPT_FILENAME  $document_root$fastcgi_script_name;
}
```

4.:

Der `www-data` Nutzer benötigt Leserechte auf die `core_reaction` Tabelle in der Datenbank.
